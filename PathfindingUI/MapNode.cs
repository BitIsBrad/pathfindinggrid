﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathfindingUI
{

    //The class representing a Node within Grid
    //Contains location (2D)
    //Contains parameters for A* pathfinding
    class MapNode
    {

        //2D location
        public uint X;
        public uint Y;

        //Flag to indicate accessability
        public bool Walkable;

        //Distance holder for heuristic
        public double Distance;

        //Parent refrence
        public int ParentIndex = -1;

        //Score for graph traversal in pathfinding
        public uint Score = 0;

        //Instanciation, node location + optional walkability flag (default true=walkable)
        public MapNode(uint x, uint y, bool walkable = true)
        {
            X = x;
            Y = y;
            Walkable = walkable;
        }

        //Calculate distance heuristic for given target
        public void UpdateDistance(uint toX, uint toY)
        {
            //Pythagorous
            //Could be simplified for preformance at the cost of path quaility
            Distance = Math.Sqrt(Math.Pow(Math.Abs(toX - X), 2) + Math.Pow(Math.Abs(toY - Y), 2));
        }

        //Static helper for creating a group of nodes denoting a 2D grid
        //Params : width and height
        //Return : List of nodes in grid
        public static List<MapNode> MakeGrid(uint width, uint height)
        {
            //Create new output list
            List<MapNode> map = new List<MapNode>();
            //iterate over all grid locations
            for (uint x = 0; x < width; x++)
            {
                for (uint y = 0; y < height; y++)
                {
                    //Create MapNode with location
                    map.Add(new MapNode(x, y));
                }
            }
            //Return list
            return map;
        }

    }
}
