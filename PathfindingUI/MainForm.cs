﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PathfindingUI
{
    //Operations run on the UI thread!!!
    //This is terrible but demonstrates the algorithm sufficiently.

    public partial class MainForm : Form
    {
        //2D map object
        Map map = new Map();

        //Start and enpoints store
        int startPoint = -1;
        int endPoint = -1;

        public MainForm()
        {
            InitializeComponent();
        }

        //Draw the map (2D grid) to bitmap and load into picturebox
        public void DrawMap()
        {

            gridBox.Image = map.ToBitmap(gridBox.Width, gridBox.Height);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //Startup the visualiser
            DrawMap();
        }

        //Moniter for clicks on the grid
        private void gridBox_Click(object sender, EventArgs e)
        {
            //Cast event to MouseEvent for location data
            MouseEventArgs click = e as MouseEventArgs;
            //Lookup pixel location in grid
            int pos = map.GetNodeAtReal(gridBox.Width, gridBox.Height, click.X, click.Y);
            
            //Check if needing a start point or end point
            if (startPoint == -1)
            {
                startPoint = pos;
            } else
            {
                endPoint = pos;
                //If start point and end point find path and draw
                if (map.CalculatePath(startPoint, endPoint))
                {
                    DrawMap();
                } else
                {
                    //Report failure
                    MessageBox.Show("Finding path failed, check start and end are accessable");
                }
                
                //Reset for next path
                startPoint = -1;
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            map.Paths.Clear();
            DrawMap();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            map = new Map(GetWidth(), GetHeight(), false);
            map.RandomiseMap(GetNoise());
            DrawMap();
        }

        private uint GetHeight()
        {
            uint value = 64;
            uint.TryParse(heightTextBox.Text, out value);
            if (value > 150) value = 150;
            if (value < 5) value = 5;
            heightTextBox.Text = value.ToString();
            return value;
        }

        private uint GetWidth()
        {
            uint value = 64;
            uint.TryParse(widthTextBox.Text, out value);
            if (value > 150) value = 150;
            if (value < 5) value = 5;
            widthTextBox.Text = value.ToString();
            return value;
        }

        private uint GetNoise()
        {
            uint value = 40;
            uint.TryParse(noiseTextBox.Text, out value);
            if (value > 100) value = 100;
            noiseTextBox.Text = value.ToString();
            return value;
        }

        private void MainForm_ResizeEnd(object sender, EventArgs e)
        {
            DrawMap();
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            DrawMap();
        }
    }
}
