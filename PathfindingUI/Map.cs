﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathfindingUI
{
    //The map class holds the grid in memory.
    //The class presents an interface for interacting with the grid
    //Pathfinding is embedded into the class
    class Map
    {

        //Width and Height of the grid
        public readonly uint Width;
        public readonly uint Height;

        //In memory storage of the grid locations
        public List<MapNode> Nodes;
        //In memory storage of paths found by the pathfinding routine
        public List<List<int>> Paths = new List<List<int>>();

        //Class instanciation
        public Map(uint width = 64, uint height = 64, bool randomise = true)
        {
            Width = width;
            Height = height;
            Nodes = MapNode.MakeGrid(width, height);
            if (randomise) RandomiseMap(30);
        }

        //Return the list index of a given grid position,
        //Returns -1 if element is not found or is outside the grid
        public int GetNodeIndex(uint x, uint y)
        {
            //Currently heavily unoptimised
            //In range checks should be done first to reject outliers
            //SortedList used with binary search to improve speed
            for (int index = 0; index < Nodes.Count; index++)
            {
                if (Nodes[index].X == x && Nodes[index].Y == y) return index;
            }
            return -1;
        }

        //Add random terrain to the map
        //Noise is a percentage of unaccessable terrain
        public void RandomiseMap(uint noise)
        {

            //Number of elements to create
            noise = (uint)(Nodes.Count * ((double)noise / 100));

            Random sequence = new Random();

            //Will not gaurentee a given amount of noise due to random revisiting locations.
            for (uint i = 0; i < noise; i++)
            {
                Nodes[sequence.Next(Nodes.Count)].Walkable = false;
            }

        }

        //Calculate heurstic for entire grid given a target location
        private void PrecalculateDistance(uint toX, uint toY)
        {
            //Preform sanity check to ensure target is within grid
            if (toX < Width && toY < Height)
            {
                foreach (MapNode node in Nodes)
                {
                    //Delegate heuristic calculation to MapNode's routine
                    node.UpdateDistance(toX, toY);
                }
            }
        }

        //Return list of element indicies around a given point in grid
        private List<int> GetAround(int center)
        {
            //Declare output list
            List<int> around = new List<int>();

            //Iterate -1 -> +1 around the center points x and y coords
            for (int x = ((int)Nodes[center].X) - 1; x <= ((int)Nodes[center].X) + 1; x++)
            {
                for (int y = ((int)Nodes[center].Y) - 1; y <= ((int)Nodes[center].Y) + 1; y++)
                {
                    //Reject center itself
                    if ((uint)x == Nodes[center].X && (uint)y == Nodes[center].Y) continue;
                    //Reject points outside of the grid's boundries
                    if ((uint)x < 0 || (uint)x >= Width || (uint)y < 0 || (uint)y >= Height) continue;
                    //Establish the locations index within node list
                    int index = GetNodeIndex((uint)x, (uint)y);
                    //Add to output list providing location is accessable (walkable) and does not intersect another path
                    //Currently checking if path intersects at ANY given time.
                    if (Nodes[index].Walkable && IsOnPath(index) == -1) around.Add(index);
                }
            }
            return around;
        }

        //Calculate path between two points
        //Returns true/false depending on success
        //Params: to, from = indecies of points within grid list
        public bool CalculatePath(int from, int to)
        {
            //Empty the previous paths list
            Paths.Clear();
            //Find path links and update grid nodes in global list
            int end = FindPath(Nodes[from].X, Nodes[from].Y, Nodes[to].X, Nodes[to].Y);

            //Check if this was successfull
            if (end != -1)
            {
                //Trace path backwards from end and add to paths list
                Paths.Add(TracePath(end));
                //Report success
                return true;
            }
            else
            {
                //Report failure
                return false;
            }
        }

        //Trace path from end node and calculated links in global list
        //Will lock and overflow if not run before FindPath!!!
        //Param: startIndex = grid node index of location at end of found path
        private List<int> TracePath(int startIndex)
        {
            //Declare output list
            List<int> path = new List<int>();
            //Push initial location (end)
            path.Add(startIndex);

            //Iterativly add the next node in the list
            do
            {
                //Load the node that points to current location
                startIndex = Nodes[startIndex].ParentIndex;
                path.Add(startIndex);

                //Finish when a node without a previous location is found
            } while (Nodes[startIndex].ParentIndex != -1);

            //Reverse path so its start -> end
            path.Reverse();
            return path;
        }


        //Find Path from a given start point to an end point
        //Returns -1 if a path cannot be found or start/end combo is invalid
        private int FindPath(uint fromX, uint fromY, uint toX, uint toY)
        {
            //If start and end are the same abort
            if (fromX == toX && fromY == toY) return -1;

            //Find index of start location grid node
            int fromIndex = GetNodeIndex(fromX, fromY);

            //Signify the start location by given the node no parent
            Nodes[fromIndex].ParentIndex = -1;

            //Find index of end location grid node
            int toIndex = GetNodeIndex(toX, toY);

            //Check that both the start and the end location are accessable
            if (!Nodes[fromIndex].Walkable || !Nodes[toIndex].Walkable) return -1;

            //Precaculate the Heuristic for distance estimation for all nodes
            //Preformace could be improved by calculating this on demand
            PrecalculateDistance(toX, toY);

            //List of nodes (locations) that are to be checked
            List<int> open = new List<int>();

            //List of nodes (locations) that have been checked and should not be revisited
            List<int> closed = new List<int>();

            //Add start location to the open (search queue) list
            open.Add(fromIndex);

            do
            {
                //Find the node closest to target in work queue
                int openIndex = GetLowestDistanceIndex(open);

                //Return the node index of node in open list
                int currentIndex = open[openIndex];
                //Remove the node from todo (open) list
                open.RemoveAt(openIndex);
                //Add the Node to the closed (done) list
                closed.Add(currentIndex);

                //Check if the node is the target node
                if (Nodes[currentIndex].X == toX && Nodes[currentIndex].Y == toY)
                {
                    //If so return early with index of grid location node
                    return currentIndex;
                }

                //Find the walkable nodes around the location
                //Todo : add time data to get arround so paths do not cross in respect to time of movement
                List<int> around = GetAround(currentIndex);

                //Iterate over all walkable nodes around current location
                foreach (int index in around)
                {
                    //Skip if the location has already been checked for children
                    if (closed.Contains(index)) continue;

                    //Check if the node has been seen before on alternative path
                    if (open.Contains(index))
                    {
                        //Check if the node's score is better than previously seen
                        if (Nodes[currentIndex].Score + 1 < Nodes[index].Score)
                        {
                            //If so, update with new (lower score)
                            Nodes[index].Score = Nodes[currentIndex].Score + 1;
                            //Update the parent of the new node (current location)
                            Nodes[index].ParentIndex = currentIndex;
                        }
                    }
                    else
                    {
                        //If new location then update the distance score
                        Nodes[index].Score = Nodes[currentIndex].Score + 1;
                        //Store the parent of the new node (current location)
                        Nodes[index].ParentIndex = currentIndex;
                        //Add the node to work queue (open list)
                        open.Add(index);
                    }
                }

                //Loop until all paths have been followed
            } while (open.Count > 0);

            //If all paths where checked and target was not found report failure
            return -1;
        }

        //Get node with best distance score to target (from list of map node indicies)
        //List must have at least one element
        private int GetLowestDistanceIndex(List<int> nodes)
        {

            //Index of best location in global grid list
            int bestGlobalIndex = nodes[0];
            //Index of best location in node list param
            int bestListIndex = 0;

            //Iterate over all nodes in list provided
            for (int listIndex = 0; listIndex < nodes.Count; listIndex++)
            {
                //Check if the score is better than the lowest seen so far
                int globalIndex = nodes[listIndex];
                if (Nodes[globalIndex].Distance < Nodes[bestGlobalIndex].Distance)
                {
                    //If so update the current best score
                    bestGlobalIndex = globalIndex;
                    bestListIndex = listIndex;
                }
            }

            //Return the index of the best node in list provided (param)
            return bestListIndex;
        }

        //Check if a location in grid (by index) is on any path at given or any time
        //Params : nodeIndex : location on grid via index in global list, timeSlot : time at which to check (-1 = any/all time(s))
        //Return : index of path where collision occours OR -1 if no intersection
        private int IsOnPath(int nodeIndex, int timeSlot = -1)
        {
            //Todo : implement time contraint

            //Iterate over all paths stored
            for (int pathsIndex = 0; pathsIndex < Paths.Count; pathsIndex++)
            {
                //Iterate over all steps in given path
                foreach (int pathNode in Paths[pathsIndex])
                {
                    //If the node intersects path return the path node index in list
                    if (pathNode == nodeIndex) return pathsIndex;
                }
            }
            //If none found return -1;
            return -1;
        }

        //Return the node index in global grid at given pixel location (in refrence to output bitmap via ToBitmap)
        public int GetNodeAtReal(int width, int height, int x, int y)
        {
            //Calculate maximium scaling factors for Width and height
            int xScale = width / (int)Width;
            int yScale = height / (int)Height;
            //Select lowest scaling factor to ensure even scaling
            int scale = Math.Min(xScale, yScale);

            //Declare holders for offset for center
            int xOffset = 0;
            int yOffset = 0;

            //Calculate offset values for given size
            xOffset = (width - (scale * (int)Width)) / 2;
            yOffset = (height - (scale * (int)Height)) / 2;

            //Calculate real location in respect to pixel location provided
            x = (x  - xOffset) / scale;
            y = (y  - yOffset) / scale;

            //Check the location is within the boundries of the grid
            if (x > Width || y > Height || x < 0 || y < 0) return -1;

            //Return the Grid index at given location
            return GetNodeIndex((uint)x, (uint)y);
        }

        //Return a bitmap of given size representing the 2D grid
        public Bitmap ToBitmap(int width, int height)
        {
            //Instanciate a Bitmap of desired size
            Bitmap canvas = new Bitmap(width, height);

            //Calculate maximium scaling factors for Width and height
            int xScale = width / (int)Width;
            int yScale = height / (int)Height;
            //Select lowest scaling factor to ensure even scaling
            int scale = Math.Min(xScale, yScale);

            //Declare holders for offset for center
            int xOffset = 0;
            int yOffset = 0;

            //Calculate offset values for given size
            xOffset = (width - (scale * (int)Width)) / 2;
            yOffset = (height - (scale * (int)Height)) / 2;
 
            //Wrap in using block to ensure correct disposing of graphics instance upon completion
            using (Graphics graphics = Graphics.FromImage(canvas))
            {
                //Set background color
                graphics.Clear(Color.LightGray);
                
                //Iterate over all locations within the grid
                //Todo : change this to direct list iteration for preformance increase
                for (int x = 0; x < Width; x++)
                {
                    for (int y = 0; y < Height; y++)
                    {
                        //Get node index at given location
                        int nodeIndex = GetNodeIndex((uint)x, (uint)y);

                        //Create rectange of size and location for Grid location
                        Rectangle block = new Rectangle(
                            xOffset + (x * scale), 
                            yOffset + (y * scale),
                            scale,
                            scale
                            );

                        //Set draw color dependant upon walkability and existance on a path
                        if (Nodes[nodeIndex].Walkable)
                        {
                            int pathIndex = IsOnPath(nodeIndex);
                            if (pathIndex == -1)
                            {
                                graphics.FillRectangle(Brushes.White, block);
                            }
                            else
                            {
                                graphics.FillRectangle(Brushes.Orange, block);
                            }

                        }
                        else
                        {
                            graphics.FillRectangle(Brushes.Black, block);
                        }
                    }
                }

            }

            //Return genorated bitmap
            return canvas;
        }

        //ToString functionality for commandline testing/debugging
        //Writes the grid to a string object
        //Paths will be denoted by letters denoting which path (eg 0 = A, 1 = B)
        public override string ToString()
        {
            string output = '+' + new string('-', (int)Width) + '+';
            output += '\n';
            for (uint y = 0; y < Height; y++)
            {
                output += '|';
                for (uint x = 0; x < Width; x++)
                {
                    int nodeIndex = GetNodeIndex(x, y);
                    if (Nodes[nodeIndex].Walkable)
                    {
                        int pathIndex = IsOnPath(nodeIndex);
                        if (pathIndex == -1)
                        {
                            output += ' ';
                        }
                        else
                        {
                            output += Convert.ToChar(65 + pathIndex);
                        }

                    }
                    else
                    {
                        output += '#';
                    }
                }
                output += "|\n";
            }
            output += '+' + new string('-', (int)Width) + '+';
            return output;
        }

    }
}
